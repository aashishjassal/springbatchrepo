package com.cars24.postAuction.postAuctionService;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class JobLauncherController {

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	@Qualifier("AashishJob")
	Job job;

	@GetMapping("/jobLauncher")
	public ResponseEntity handle() throws Exception {
		final JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
				.toJobParameters();
		final JobExecution run = jobLauncher.run(job, jobParameters);
		System.out.println("Steps status:" + run.getStepExecutions());
		return ResponseEntity.ok().body("ok");
	}
}