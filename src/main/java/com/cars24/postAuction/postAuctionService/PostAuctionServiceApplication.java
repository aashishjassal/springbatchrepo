package com.cars24.postAuction.postAuctionService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostAuctionServiceApplication {


	public static void main(String[] args) {
		SpringApplication.run(PostAuctionServiceApplication.class, args);
	}



}
