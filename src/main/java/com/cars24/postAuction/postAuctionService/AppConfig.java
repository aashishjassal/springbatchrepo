package com.cars24.postAuction.postAuctionService;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
@EnableBatchProcessing

public class AppConfig {

	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private TaskExecutor taskExecutor;

	@Bean
	public TaskExecutor taskExecutor() {
		return new SimpleAsyncTaskExecutor("aashish");
	}

	@Bean(name = "AashishJob")
	public Job job(@Qualifier("step1") Step step1, @Qualifier("step2") Step step2) {

		return jobs.get("job").incrementer(new RunIdIncrementer()).start(splitFlow()).next(step4()).build() // builds
																											// FlowJobBuilder
																											// instance
				.build(); // builds Job instance
//		return jobs.get("myJob").start(step1).next(step2).listener(new JobExecutionListener() {
//
//			@Override
//			public void beforeJob(JobExecution jobExecution) {
//				System.out.println("beforeJob");
//			}
//
//			@Override
//			public void afterJob(JobExecution jobExecution) {
//				System.out.println("afterJob");
//
//			}
//		}).build();
	}

	@Bean(name = "step2")
	protected Step step2() {
		return stepBuilderFactory.get("step2").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				System.out.println("Step 2 tasklet");
				TimeUnit.SECONDS.sleep(5);
				System.out.println("Step 2 tasklet done");
				return RepeatStatus.FINISHED;
			}
		}).build();
	}

	@Bean(name = "step1")
	protected Step step1() {
		return stepBuilderFactory.get("step1").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				System.out.println("Step 1 tasklet");
				TimeUnit.SECONDS.sleep(5);
				System.out.println("Step 1 tasklet done");
				return RepeatStatus.FINISHED;
			}
		}).build();
	}

	@Bean(name = "step3")
	protected Step step3() {
		return stepBuilderFactory.get("step3").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				System.out.println("Step 3 tasklet");
				TimeUnit.SECONDS.sleep(5);
				System.out.println("Step 3 tasklet done");
				return RepeatStatus.FINISHED;
			}
		}).build();
	}

	protected Step step4() {
		return stepBuilderFactory.get("step4").tasklet(new Tasklet() {

			@Override
			public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				final RetryTemplate retryTemplate = new RetryTemplate();
				final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
				retryPolicy.setMaxAttempts(3);
				retryTemplate.setRetryPolicy(retryPolicy);
				final AtomicInteger c = new AtomicInteger(1);
				final RepeatStatus execute = retryTemplate.execute(new RetryCallback<RepeatStatus, Exception>() {

					@Override
					public RepeatStatus doWithRetry(RetryContext context) throws RuntimeException {
						System.out.println("executing retry: " + context.getRetryCount());
						if (c.get() > 2) {

							System.out.println("Step 4 tasklet");
							try {
								TimeUnit.SECONDS.sleep(5);
							} catch (final InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.println("Step 4 tasklet done");
							return RepeatStatus.FINISHED;
						} else {
							c.incrementAndGet();
							throw new RuntimeException("Not yet");
						}
					}
				});
				System.out.println("Now returning");
				return execute;
			}
		}).build();
	}

	@Bean
	public Flow splitFlow() {
		return new FlowBuilder<SimpleFlow>("splitFlow").split(taskExecutor).add(flow1(), flow2()).build();
	}

	@Bean
	public Flow flow1() {
		return new FlowBuilder<SimpleFlow>("flow1").start(step1()).next(step2()).build();
	}

	@Bean
	public Flow flow2() {
		return new FlowBuilder<SimpleFlow>("flow2").start(step3()).build();
	}
}
